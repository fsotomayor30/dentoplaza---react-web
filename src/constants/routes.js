export const LANDING = '/';
export const NUESTRACLINICA = '/nuestraclinica';
export const PACIENTES = '/pacientes';
export const PROFESIONALES = '/profesionales';
export const INGRESARPACIENTE = '/ingresarpaciente';
export const VERPACIENTE = '/paciente/:id';
export const TRATAMIENTOS = '/tratamientos/:id';


export const HOME = '/home';
export const SIGN_UP = '/signup';
export const SIGN_IN = '/signin';
export const ACCOUNT = '/account';
export const ADMIN = '/admin';
export const PASSWORD_FORGET = '/pw-forget';
export const PASSWORD_CHANGE = '/pw-change';
export const DESAYUNOS = '/desayunos';
export const ALMUERZOS = '/almuerzos';
export const SANDWITCH = '/sandwitches';
export const INGRESODESAYUNO = '/ingreso-desayuno';
export const INGRESOALMUERZO = '/ingreso-almuerzo';
export const INGRESOSANDWITCH = '/ingreso-sandwitch';
export const ELIMINARDESAYUNO = '/eliminar-desayuno';
export const ELIMINARALMUERZO = '/eliminar-almuerzo';
export const ELIMINARSANDWITCH = '/eliminar-sandwitch';
export const COMPRAS = '/carro-compras';
export const COCINA = '/cocina';

