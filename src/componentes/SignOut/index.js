import React from 'react';
import '../../App.css';
import logout from '../../imagenes/logout.png';
import {Navbar, Nav, NavDropdown, Badge} from 'react-bootstrap';


import { withFirebase } from '../Firebase';

const SignOutButton = ({ firebase }) => (
  <>
    <Nav>
        <button type="submit" className="btn_2  d-lg-block" onClick={firebase.doSignOut}>
            Cerrar Sesión

      </button>
    </Nav>


  </>
);

export default withFirebase(SignOutButton);