import React from 'react';
import { AuthUserContext } from '../Session';
import {Navbar, Nav, NavDropdown, Badge} from 'react-bootstrap';
import SignOutButton from '../SignOut';
import * as ROUTES from '../../constants/routes';
import {Link} from 'react-router-dom'


import { withFirebase } from '../Firebase';

import logo from '../../img/dentoplaza.jpeg';




import * as ROLES from '../../constants/roles';
import SignOut from '../SignOut';


const Navigation = () => (
  <AuthUserContext.Consumer>
    {authUser =>
      authUser ? (
        <NavigationAuth authUser={authUser} />
      ) : (
        <NavigationNonAuth />
      )
    }
  </AuthUserContext.Consumer>
);


const NavigationAuth = ({ authUser}) => (
  <header className="main_menu">
        <Navbar collapseOnSelect expand="lg" >
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                <a className="navbar-brand" href={ROUTES.LANDING}> <img src={logo} height="120" lt="logo"/> </a>

                    <Nav className="mr-auto">

                        <Nav.Link href={ROUTES.LANDING}>Inicio</Nav.Link>
                        <Nav.Link className="division" >|</Nav.Link>
                        <Nav.Link href={ROUTES.NUESTRACLINICA}>Nuestra Clínica</Nav.Link>
                        <Nav.Link  className="division">|</Nav.Link>
                        <Nav.Link href={ROUTES.PROFESIONALES}>Profesionales</Nav.Link>
                        <Nav.Link className="division">|</Nav.Link>
                        <Nav.Link href={"/tratamientos/"+authUser.uid} >Mis Tratamientos</Nav.Link>
                        <Nav.Link className="division">|</Nav.Link>
                        <Nav.Link href={"/#contactanos"} >Contáctanos</Nav.Link>
                        <Nav.Link className="division">|</Nav.Link>
                        <Nav.Link href={"/#servicios"} >Servicios</Nav.Link>

                        {!!authUser.roles[ROLES.ADMIN] && (
                                    <>
                                        <Nav.Link className="division">|</Nav.Link>                                                                            
                                        <NavDropdown title="Administración" id="collasible-nav-dropdown">
                                          <NavDropdown.Item  href={ROUTES.ADMIN}>Usuarios</NavDropdown.Item >
                                          <NavDropdown.Item  href={ROUTES.PACIENTES}>Pacientes</NavDropdown.Item >
                                        </NavDropdown>

                                    </>
                        )}
            
                    </Nav>
                    <SignOut/>
                </Navbar.Collapse>
            </Navbar>
    </header>

);

const NavigationNonAuth = () => (
  <header className="main_menu ">
            <Navbar collapseOnSelect expand="lg" >
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                    <a className="navbar-brand" href={ROUTES.LANDING}> <img src={logo} height="120" lt="logo"/> </a>

                        <Nav className="mr-auto">
                            <Nav.Link href={ROUTES.LANDING}>Inicio</Nav.Link>
                            <Nav.Link className="division">|</Nav.Link>
                            <Nav.Link href={ROUTES.NUESTRACLINICA}>Nuestra Clínica</Nav.Link>
                            <Nav.Link className="division">|</Nav.Link>
                            <Nav.Link href={ROUTES.PROFESIONALES}>Profesionales</Nav.Link>
                            
                        </Nav>
                        <Nav>
                          <Link to={ROUTES.SIGN_IN}>
                              <button type="submit" className="btn_2  d-lg-block">
                                Iniciar Sesión
                              </button>                            
                            </Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>  

  </header>

  
);



export default Navigation;