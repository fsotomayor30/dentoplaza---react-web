import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';

import { withFirebase } from '../Firebase';
import * as ROUTES from '../../constants/routes';

const SignInPage = () => (
  <div className="contenido">
    <SignInForm />
    {/* <PasswordForgetLink /> */}
    {/* <SignUpLink /> */}
  </div>
);

const INITIAL_STATE = {
  email: '',
  password: '',
  error: null,
};

class SignInFormBase extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  onSubmit = event => {
    const { email, password } = this.state;

    this.props.firebase
      .doSignInWithEmailAndPassword(email, password)
      .then(() => {
        this.setState({ ...INITIAL_STATE });
        this.props.history.push(ROUTES.LANDING);
      })
      .catch(error => {
        this.setState({ error });
      });

    event.preventDefault();
  };

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { email, password, error } = this.state;

    const isInvalid = password === '' || email === '';

    return (
      <>
      <form onSubmit={this.onSubmit} className="row form">

      <div className="fondo"></div>

      <div className="grad"></div>
      <div className="header col-sm-12 col-lg-6">
        <div>Dento<span>Plaza</span></div>
      </div>
      <br/>
    
		<div className="login col-sm-12 col-lg-6">
          <input id="email" type="text" className="input" name="email"
          value={email}
          onChange={this.onChange}
          type="text"
          placeholder="Correo electrónico"/><br/>
				  <input id="pass" className="input" data-type="password" name="password"
          value={password}
          onChange={this.onChange}
          type="password"
          placeholder="Contraseña"/><br/>
				<input type="submit" disabled={isInvalid}  value="Iniciar Sesión"/>
        
        <div className="footer-login">
          <p style={{color:"white"}}>
            Olvidaste la clave? <a href={ROUTES.PASSWORD_FORGET}>Click aquí</a>
          </p>
          <p style={{color:"white"}}>
          No tienes una cuenta? <a href={ROUTES.SIGN_UP}>Registrate</a>
          </p>
				</div>

		</div> 
    </form>
        {error && <div className="alert alert-danger error">
        {error.message}</div>}
      
</>
    );
  }
}

const SignInForm = compose(
  withRouter,
  withFirebase,
)(SignInFormBase);

export default SignInPage;

export { SignInForm };