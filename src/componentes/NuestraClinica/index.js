import React from 'react';
import banner from '../../img/banner_img.png';
import mapa from '../../img/mapa.png';
import ubicacion from '../../img/ubicacion.png';
import movilizacion from '../../img/movilizacion.png';
import atencion from '../../img/atencion.png';
import diente from '../../img/icon/feature_1.svg';
import service from '../../img/service.png';

import doc1 from '../../img/doctor/doctor_1.png';
import doc2 from '../../img/doctor/doctor_2.png';
import doc3 from '../../img/doctor/doctor_3.png';
import doc4 from '../../img/doctor/doctor_4.png';
import noimagen from '../../img/noimagen.png';




const nuestraClinica = () => (
  <div>
      <span className="clearfix"></span>
    <section className="breadcrumb_part breadcrumb_bg">
        <div className="container">
            <div className="row">
                <div className="col-lg-12">
                    <div className="breadcrumb_iner">
                        <div className="breadcrumb_iner_item">
                            <h2>Nuestra Clínica</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section className="about_us single_about_padding">
        <div className="container">
            <div className="row justify-content-between align-items-center">
                <div className="col-md-6 col-lg-6">
                    <div className="about_us_img">
                        <img src={noimagen} alt=""/>
                    </div>
                </div>
                <div className="col-md-6 col-lg-5">
                    <div className="about_us_text">
                        <h2><span className="dento">Dento</span><span className="plaza">plaza</span></h2>
                        <p>En Clínica DentoPlaza estamos comprometidos con tu salud bucal, brindando una cálida y personalizada atención.
                                Contamos con tratamientos integrales realizados por profesionales y/o especialistas en el area de la Odontología utilizando excelentes materiales y infraestructura (incluyendo sala de rayos X).</p>
                        <p>La clínica dispone de cómodas salas de espera y espacio para niños.
                            Todas nuestras instalaciones están autorizadas por el Servicio de Salud Ñuble (MINSAL).</p>
                        <a className="btn_2 " href="#">Leer más</a>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>

   

    </div>
);

export default nuestraClinica;