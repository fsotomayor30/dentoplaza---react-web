import React, { Component } from 'react';
import { withAuthorization } from '../Session';
import {Nav, Table } from 'react-bootstrap';
import desayuno from '../../imagenes/desayuno.png';
import almuerzo from '../../imagenes/almuerzo.png';
import sandwitch from '../../imagenes/sandwitch.png';
import * as ROLES from '../../constants/roles';
import Spinner from 'react-spinner-material';
import admin from '../../imagenes/admin.png';
import mas from '../../imagenes/mas.png';
import * as ROUTES from '../../constants/routes';

class eliminarDesayuno extends React.Component {
  

  constructor(props) {
    super(props);
    this.state = { 
        loading: false,
        desayunos: [],
     };
  }

  componentWillUnmount() {
    this.props.firebase.desayunos().off();
  }

  componentDidMount() {
    this.setState({ loading: true });

    this.props.firebase.desayunos().on('value', snapshot => {
      const desayunoObject = snapshot.val();

      if (desayunoObject) {
        const desayunoList = Object.keys(desayunoObject).map(key => ({
          ...desayunoObject[key],
          uid: key,
        }));

        this.setState({desayunos: desayunoList,
          loading: false });
      } else {
        this.setState({ desayunos: null, loading: false });
      }
    });
  }

  eliminarDesayuno = event => {

    this.props.firebase.desayuno(event.target.value).remove();

    this.setState({
    
    })


  
  event.preventDefault();
}

  render() {
    const { desayunos, loading } = this.state;
    

    return (
      <div className="contenido">
          {loading && <div className="spinnerP">
          <Spinner size={120} spinnerColor={"#4a6fa6"} spinnerWidth={2} visible={true} />
          <p><img className="logo" src={admin} alt="almuerzo"/>Estamos cargando los desayunos . . . </p>
            </div>}
        
            <Nav fill variant="tabs" className="tabs" defaultActiveKey="/eliminar-desayuno">
                <Nav.Item>
                    <Nav.Link href="/eliminar-desayuno"><img src={desayuno}/>Desayunos</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link href="/eliminar-almuerzo"><img src={almuerzo}></img>Almuerzos</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link href="/eliminar-sandwitch"><img src={sandwitch}/>Sandwitch</Nav.Link>
                </Nav.Item>
    
            </Nav> 

            {desayunos ? (
            <Table striped bordered hover variant="dark">
                <thead>
                    <tr>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Precio</th>
                    <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {desayunos.map(desayuno => (
                    <tr key={desayuno.uid}>
                        <td> {desayuno.nombre}</td>
                        <td> {desayuno.descripcion}</td>
                        <td> $ {desayuno.precio}</td>
                        <td>
                        <button type="submit" value={desayuno.uid} className="sign-out" onClick={this.eliminarDesayuno} >
                        Eliminar
                            </button> 
                        </td>
                        
                    </tr>
                    ))}
                </tbody>
            </Table>
            ) : (
                <div className="no-contenido dark">
                    <p>No hay ningún desayuno disponible ...</p>
                    <p className="agregar">Agregar un desayuno <a href={ROUTES.INGRESODESAYUNO} className="agregar" >
                        <img src={mas}/></a>
                    </p>
                </div>
              )}
        </div>
    );
  }
}

const condition = authUser =>
  authUser && !!authUser.roles[ROLES.ADMIN];
  
export default withAuthorization(condition)(eliminarDesayuno);