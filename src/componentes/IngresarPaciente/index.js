import React from 'react';
import { withAuthorization } from '../Session';
import {Form, Button, FormControl,InputGroup, Nav } from 'react-bootstrap';

import * as ROLES from '../../constants/roles';
import menos from '../../imagenes/eliminar.png';
import mas from '../../imagenes/mas.png';
import * as ROUTES from '../../constants/routes';
import paciente from '../../img/paciente.png';
import ubicacion from '../../img/ubicacion.png';
import rut from '../../img/rut.png';
import sexo from '../../img/sexo.png';
import email from '../../img/email.png';
import telefono from '../../img/telefono.png';

class IngresoPaciente extends React.Component {
  

  constructor(props) {
    super(props);
    this.state = { 
      nombrepaciente: '',
      direccionpaciente: '',
      rutpaciente: '',
      sexopaciente: '',
      email:'',
      telefonopaciente:'',
      idPaciente:'',
      emails: [],
      validated: false,
      mensaje: null,
      mensaje2: null

     };
  }

  componentDidMount() {

    this.props.firebase.users().on('value', snapshot => {
      const messageObject = snapshot.val();

      if (messageObject) {
        const messageList = Object.keys(messageObject).map(key => ({
          ...messageObject[key],
          uid: key,
        }));

        this.setState({emails: messageList
           });
      } else {
        this.setState({ emails: null});
      }
    });
  }

  onCreateMessage = event => {

    if(!(this.state.nombrepaciente === '' && this.state.nombrepaciente === '' && this.state.rutpaciente ==='')){
      this.props.firebase.paciente(this.state.idPaciente).set({
        nombre: this.state.nombrepaciente,
        direccion: this.state.direccionpaciente,
        rut: this.state.rutpaciente,
        sexo: this.state.sexopaciente,
        email: this.state.email,
        telefono: this.state.telefonopaciente
      });

      this.setState({
        mensaje: "Paciente agregado con éxito"
      })

      setTimeout( () => {this.setState({ 
        mensaje:null

     })}, 2000);
     this.props.history.push(ROUTES.PACIENTES);

  }else{
    this.setState({
      mensaje2: "Tienes que completar todos los campos"
    })
    
    setTimeout( () => {this.setState({ 
      mensaje2:null
   })}, 2000);
  }

    event.preventDefault();
  };

  
  onChangeText = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  onChangeUser = event => {
    this.setState({ [event.target.name]: event.target.value });
    this.state.emails.map((user, i) => {
      if(user.email === event.target.value){
        this.setState({ idPaciente: user.uid});
        //console.log(user.uid);

      }
    })
  };

  onChangeSexo = event => {
    this.setState({ [event.target.name]: event.target.value });

  };
  

  checkRut = event => {
    // Despejar Puntos
    var valor = event.target.value.replace('.','');
    // Despejar Guión
    valor = valor.replace('-','');
    
    // Aislar Cuerpo y Dígito Verificador
    var cuerpo = valor.slice(0,-1);
    var dv = valor.slice(-1).toUpperCase();
    
    // Formatear RUN
    event.target.value = cuerpo + '-'+ dv
    
    // Si no cumple con el mínimo ej. (n.nnn.nnn)
    if(cuerpo.length < 7) { event.target.setCustomValidity("RUT Incompleto"); return false;}
    
    // Calcular Dígito Verificador
    var suma = 0;
    var multiplo = 2;
    
    // Para cada dígito del Cuerpo
    for(var i=1;i<=cuerpo.length;i++) {
    
        // Obtener su Producto con el Múltiplo Correspondiente
        var index = multiplo * valor.charAt(cuerpo.length - i);
        
        // Sumar al Contador General
        suma = suma + index;
        
        // Consolidar Múltiplo dentro del rango [2,7]
        if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
  
    }
    
    // Calcular Dígito Verificador en base al Módulo 11
    var dvEsperado = 11 - (suma % 11);
    
    // Casos Especiales (0 y K)
    dv = (dv == 'K')?10:dv;
    dv = (dv == 0)?11:dv;
    
    // Validar que el Cuerpo coincide con su Dígito Verificador
    if(dvEsperado != dv) { event.target.setCustomValidity("RUT Inválido"); return false; }
    
    // Si todo sale bien, eliminar errores (decretar que es válido)
    event.target.setCustomValidity('');
}

  render() {
    const { nombrepaciente, direccionpaciente, rutpaciente, mensaje, mensaje2, sexopaciente, telefonopaciente } = this.state;
    
    return(
      <>

<section className="breadcrumb_part breadcrumb_bg">
      <div className="container">
          <div className="row">
              <div className="col-lg-12">
                  <div className="breadcrumb_iner">
                      <div className="breadcrumb_iner_item">
                          <h2>Ingresar Paciente</h2>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </section>
    <section className="doctor_part single_page_doctor_part">

  <div className="container">
        <Form
        className=""
        onSubmit={this.onCreateMessage}>  

      <Form.Group controlId="formBasicNombre">
        <Form.Label><img src={paciente} alt=""/>Nombre</Form.Label>
        <Form.Control 
          name="nombrepaciente"
          required 
          type="text" 
          value={nombrepaciente}
          placeholder="Nombre paciente"
          onChange={this.onChangeText}/>
          
      </Form.Group>

      <Form.Group controlId="formBasicDescripcion">
        <Form.Label><img src={ubicacion} alt=""/>Dirección</Form.Label>
        <Form.Control 
          required
          name="direccionpaciente"
          type="text" 
          value={direccionpaciente}
          placeholder="Dirección paciente"
          onChange={this.onChangeText} />
          
      </Form.Group>

      <Form.Group controlId="formBasicPrecio">
        <Form.Label><img src={rut} alt=""/>RUT</Form.Label>

        <Form.Control
          aria-describedby="inputGroupPrepend" 
          type="text" 
          placeholder="RUT"
          required
          value={rutpaciente}
          name="rutpaciente"
          onChange={this.onChangeText}
          onInput={this.checkRut}/>
          

      </Form.Group>

      <Form.Group controlId="formBasicPrecio">
        <Form.Label><img src={telefono} alt=""/>Teléfono</Form.Label>

        <Form.Control
          aria-describedby="inputGroupPrepend" 
          type="tel" 
          placeholder="Teléfono"
          required
          value={telefonopaciente}
          name="telefonopaciente"
          onChange={this.onChangeText}
          />
          

      </Form.Group>

      <Form.Group controlId="formBasicPrecio">
        <Form.Label><img src={sexo} alt=""/>Sexo</Form.Label>
        <Form.Control 
          value={this.state.sexo} 
          name="sexopaciente" 
          onChange={this.onChangeSexo} 
          required 
          as="select">
          
          <option >Masculino</option>
          <option >Femenino</option>
        </Form.Control>
      </Form.Group>
      {this.state.emails ? (
        <Form.Group controlId="exampleForm.ControlSelect1">
          <Form.Label><img src={email} alt=""/>Email <span className="input-explicacion">(Antes de agregar al paciente debe registrarse para ver su tratamiento)</span></Form.Label>
          <Form.Control value={this.state.email} name="email" onChange={this.onChangeUser}  as="select">
            {this.state.emails.map((user, i) => {
              return(
                
                <option key={user.uid}>{user.email}</option>
                
                )
              })}    
          </Form.Control>
        </Form.Group>
      ) : (
        <></>
      )}
      <span className="explicacion">Asegurate de presionar el botón para agregar al paciente</span>
      <div className="buttons">
              <Button variant="primary" type="submit" className="btn_2 d-lg-block">
                <img src={mas}/>Agregar Paciente
              </Button>

            </div>
</Form>  
{mensaje && <div className="alert seleccion alert-success">
          <img  src={paciente} alt="carro"/>{mensaje}</div>}
        {mensaje2 && <div className="alert seleccion alert-danger">
        <img  src={paciente} alt="carro"/>{mensaje2}</div>}</div>
        </section>
</>
);
}
}

const condition = authUser =>
  authUser && !!authUser.roles[ROLES.ADMIN];
  
export default withAuthorization(condition)(IngresoPaciente);