import {Card, Button, Row } from 'react-bootstrap';
import React, { Component } from 'react';
import { withFirebase } from '../Firebase';
import sandwitchImage from '../../imagenes/sandwitch.png';
import Spinner from 'react-spinner-material';
import dinero from '../../imagenes/dinero.png';
import { AuthUserContext } from '../Session';
import { Link } from 'react-router-dom' 
import * as ROUTES from '../../constants/routes';
import carro from '../../imagenes/carro.png';


const sandwitch = () => (
  <div className="contenido sandwitch">
    <AuthUserContext.Consumer>
    {authUser =>
      authUser ? (
        <Sandwitch authUser={authUser}/>      
      ) : (
        <Sandwitch authUser= "null"/>      
      )
    }
    </AuthUserContext.Consumer>  
  </div>
);

class SandwitchBase extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      sandwitches: [],
      tipo:"sandwitches",
      mensaje: null,
      mensajeAlerta: null
    };
  }
  componentDidMount() {
    this.setState({ loading: true });

    this.props.firebase.sandwitches().on('value', snapshot => {
      const messageObject = snapshot.val();

      if (messageObject) {
        const messageList = Object.keys(messageObject).map(key => ({
          ...messageObject[key],
          uid: key,
        }));

        this.setState({sandwitches: messageList,
          loading: false });
      } else {
        this.setState({ sandwitches: null, loading: false });
      }
    });
  }

  componentWillUnmount() {
    this.props.firebase.sandwitches().off();
  }

  agregarAlCarro  = event => {
    if(this.props.authUser === "null"){
      this.setState({
        mensajeAlerta: "Necesitas iniciar sesión"
      })
      
      setTimeout( () => {this.setState({ 
        mensajeAlerta:null
    })}, 2000); 

    }else{
    
      this.setState({idProducto: event.target.name,
        mensaje: "Sandwitch agregado con éxito" });
      
        setTimeout( () => {this.setState({ 
          mensaje:null
      })}, 2000); 

      this.props.firebase.producto(this.state.tipo,  event.target.name).on('value', test => {
        console.log(this.state.tipo+"/"+event.target.name)
        const carroObject = test.val();
        
  
         this.props.firebase.pedido(this.props.authUser.uid).push({
          nombre: carroObject.nombre,
          descripcion: carroObject.descripcion,
          precio: carroObject.precio,
          confirmada: false

        });
      }) 
    }

  }

  render() {
    const { sandwitches, loading, mensaje, mensajeAlerta } = this.state;
    return (
      <div>
        {loading && <div className="spinnerP">
          <Spinner size={120} spinnerColor={"#4a6fa6"} spinnerWidth={2} visible={true} />
          <p><img className="logo" src={sandwitchImage} alt="almuerzo"/>Estamos buscando los mejores sandwitch Gourmet para ti . . . </p>
        </div>}

        {sandwitches ? (
          <Row>
            {sandwitches.map(message => (
              <Card key={message.uid} >
                <Card.Img variant="top" src={sandwitchImage} />
                <Card.Body>
                  <Card.Title>{message.nombre}</Card.Title>
                  <Card.Text className="descripcion">
                    {message.descripcion}
                  </Card.Text>
                  <Card.Text className="precio">
                  <img src={dinero} alt="precio"/> $ {Number(message.precio).toLocaleString()}
                  </Card.Text>
                  <Button variant="primary" name={message.uid} onClick={this.agregarAlCarro}>Agregar a mi carro</Button>
                </Card.Body>
                </Card>
            ))}
            {mensaje && <div className="alert seleccion alert-success">
            <img  src={carro} alt="carro"/>{mensaje}</div>}
        {mensajeAlerta && <div className="alert seleccion alert-danger">
        {mensajeAlerta}<Link to={ROUTES.SIGN_IN}> Quieres iniciar sesión?</Link></div>}
          </Row>
          ) : (
          <div className="no-contenido">No hay ningún sandwitch disponible ...</div>
        )}
      </div>
    );
  }

  
}



const Sandwitch = withFirebase(SandwitchBase);

export default sandwitch;