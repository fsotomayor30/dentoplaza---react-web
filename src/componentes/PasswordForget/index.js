import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { withFirebase } from '../Firebase';
import * as ROUTES from '../../constants/routes';
import {Nav } from 'react-bootstrap';
import admin from '../../imagenes/admin.png';

const PasswordForgetPage = () => (
  <div>
    <PasswordForgetForm />
  </div>
);

const INITIAL_STATE = {
  email: '',
  error: null,
};

class PasswordForgetFormBase extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  onSubmit = event => {
    const { email } = this.state;

    this.props.firebase
      .doPasswordReset(email)
      .then(() => {
        this.setState({ ...INITIAL_STATE });
      })
      .catch(error => {
        this.setState({ error });
      });

    event.preventDefault();
  };

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { email, error } = this.state;

    const isInvalid = email === '';

    return (
      <>
        <form onSubmit={this.onSubmit} className="row form">
        <div className="fondo"></div>
        <div className="grad"></div>
        <div className="header col-sm-12 col-lg-6">
          <div>Dento<span>Plaza</span></div>
        </div>
        <br/>

		    <div className="login col-sm-12 col-lg-6">

					<input id="user" type="text" className="input" name="email"
          value={this.state.email}
          onChange={this.onChange}
          type="text"
          placeholder="Correo Electrónico"/><br/>
				
          <input type="submit" disabled={isInvalid} className="button" value="Recuperar contraseña"/>

        </div>

        </form>
          {error && <div className="alert alert-danger error">
          {error.message}</div>}
      
      </>
    );
  }
}

const PasswordForgetLink = () => (
  <p>
    <Link to={ROUTES.PASSWORD_FORGET}>Forgot Password?</Link>
  </p>
);

export default PasswordForgetPage;

const PasswordForgetForm = withFirebase(PasswordForgetFormBase);

export { PasswordForgetForm, PasswordForgetLink };