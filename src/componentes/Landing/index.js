import React from 'react';
import banner from '../../img/banner_img.png';
import mapa from '../../img/mapa.png';
import ubicacion from '../../img/ubicacion.png';
import movilizacion from '../../img/movilizacion.png';
import atencion from '../../img/atencion.png';
import diente from '../../img/icon/feature_1.svg';
import service from '../../img/service.png';
import dienteGrande from '../../img/dentista.png';
import telefono from '../../img/telefono.png';
import * as ROUTES from '../../constants/routes';

const landing = () => (
  <div className="landing">
    <section className="banner_part">
        <div className="container">
            <div className="row align-items-center">
                <div className="col-lg-5 col-xl-5">
                    <div className="banner_text">
                        <div className="banner_text_iner">
                            <h5>Clínica Dental</h5>
                            <h1><span className="dento">Dento</span><span className="plaza">plaza</span></h1>
                            <p>En Clínica DentoPlaza estamos comprometidos con tu salud bucal, brindando una cálida y personalizada atención.
                                Contamos con tratamientos integrales realizados por profesionales y/o especialistas en el area de la Odontología utilizando excelentes materiales y infraestructura (incluyendo sala de rayos X).
                                </p>
                            <a href={ROUTES.NUESTRACLINICA} className="btn_2">Nuestra Clínica</a>

                        </div>
                    </div>
                </div>
                <div className="col-lg-7">
                    <div className="banner_img">
                        <img src={banner} alt=""/>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section className="about_us padding_top" id="contactanos">
        <div className="container">
            <div className="row justify-content-between align-items-center">
                <div className="col-md-6 col-lg-6">
                    <div className="about_us_img">
                        <img src={mapa} alt=""/>
                    </div>
                </div>
                <div className="col-md-6 col-lg-5">
                    <div className="about_us_text">
                        <h2>¿Cómo contactarnos?</h2>
                        <p><img src={ubicacion} alt=""/><b>Dirección:</b> Calle 20 de agosto #486, Chillán Viejo<br />
                           <img src={telefono} alt=""/><b>Teléfono:</b> 42-2260862
                        </p>
                        <div className="buttons">
                            <a className="btn_2 " href="https://www.google.com/maps/place/Veinte+de+Agosto+496,+Chillan,+Chill%C3%A1n+Viejo,+Regi%C3%B3n+del+B%C3%ADo+B%C3%ADo/@-36.6233465,-72.1349126,17z/data=!3m1!4b1!4m5!3m4!1s0x9669280578800f11:0x703e864d2cda24e0!8m2!3d-36.6233465!4d-72.1327239"><img src={ubicacion} alt=""/>Ir</a>
                        </div>
                        <div className="banner_item">
                            <div className="single_item">
                                <img src={ubicacion} alt=""/>
                                <h5>Buena Ubicación</h5>
                            </div>
                            <div className="single_item">
                                <img src={movilizacion} alt=""/>
                                <h5>Buena Movilización</h5>
                            </div>
                            <div className="single_item">
                                <img src={atencion} alt=""/>
                                <h5>Buena Atención</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section className="about_us feature_part" id="servicios">
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-xl-8">
                    <div className="section_tittle text-center">
                        <h2>Nuestros servicios</h2>
                    </div>
                </div>
            </div>
            <div className="row justify-content-between align-items-center">
                <div className="col-lg-3 col-sm-12">
                    <div className="single_feature">
                        <div className="single_feature_part">
                            <span className="single_feature_icon"><img src={diente} alt=""/></span>
                            <h4>Urgencia Dental</h4>
                            <p></p>
                        </div>
                    </div>
                    <div className="single_feature">
                        <div className="single_feature_part">
                            <span className="single_feature_icon"><img src={diente} alt=""/></span>
                            <h4>Blanqueamiento Dental con láser</h4>
                            <p>Deja tus dientes relucientes.</p>
                        </div>
                    </div>
                    <div className="single_feature">
                        <div className="single_feature_part">
                            <span className="single_feature_icon"><img src={diente} alt=""/></span>
                            <h4>Limpieza Dental</h4>
                            <p>Limpiar manchas de cigarro, café, té, entre otros.</p>
                        </div>
                    </div>
                </div>
                <div className="col-lg-4 col-sm-12">
                        <div className="single_feature_img">
                            <img src={dienteGrande} alt=""/>
                        </div>
                </div>
                <div className="col-lg-3 col-sm-12">
                    <div className="single_feature">
                        <div className="single_feature_part">
                            <span className="single_feature_icon"><img src={diente} alt=""/></span>
                            <h4>Tapaduras Estéticas</h4>
                            <p>Obsturación con material de color del diente.</p>
                        </div>
                    </div>
                    <div className="single_feature">
                        <div className="single_feature_part">
                            <span className="single_feature_icon"><img src={diente} alt=""/></span>
                            <h4>Coronas (Prótesis fijas unitarias)</h4>
                            <p>Se realiza tratamiento cuando corona dental se encuentra destruida.</p>
                        </div>
                    </div>
                    <div className="single_feature">
                        <div className="single_feature_part">
                            <span className="single_feature_icon"><img src={diente} alt=""/></span>
                            <h4>Prótesis Removibles (Placa)</h4>
                            <p>Pueden ser prótesis dental de material metálica o acrílica.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    </div>
);

export default landing;