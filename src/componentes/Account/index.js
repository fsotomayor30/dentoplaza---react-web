import React from 'react';

import { AuthUserContext, withAuthorization } from '../Session';


import { PasswordForgetForm } from '../PasswordForget';
import PasswordChangeForm from '../PasswordChange';

const AccountPage = () => (
  <AuthUserContext.Consumer>
    {authUser => (
  <div className="contenido">
{/*     <h1>Cuenta: {authUser.email}</h1>
 */}    <PasswordForgetForm />
  </div>
  )}
  </AuthUserContext.Consumer>
);

const condition = authUser => !!authUser;

export default withAuthorization(condition)(AccountPage);