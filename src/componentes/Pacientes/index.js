import React, { Component } from 'react';
import {Table, Nav} from 'react-bootstrap';
import { withAuthorization } from '../Session';
import { compose } from 'recompose';
import * as ROLES from '../../constants/roles';
import { withFirebase } from '../Firebase';
import * as ROUTES from '../../constants/routes';
import { Redirect } from 'react-router-dom'
import Paciente from '../Paciente'
import ubicacion from '../../img/ubicacion.png';
import rut from '../../img/rut.png';
import mas from '../../imagenes/mas.png';
import ficha from '../../img/ficha.png';
import telefono from '../../img/telefono.png';

import {Card, Button, Row, Col, InputGroup, FormControl, Dropdown} from 'react-bootstrap';

import hombre from '../../img/paciente.png';
import mujer from '../../img/mujer.png';
import menos from '../../imagenes/eliminar.png';

import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css

class AdminPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      pacientes: [],
      buscar: '',
      eliminar: ""

    };


  }

  updateSearch(event){
    this.setState({buscar: event.target.value});
  }

  componentWillUnmount() {
    this.props.firebase.users().off();
  }

  
  componentDidMount() {
    this.setState({ loading: true });

    this.props.firebase.pacientes().on('value', snapshot => {
      const messageObject = snapshot.val();

      if (messageObject) {
        const messageList = Object.keys(messageObject).map(key => ({
          ...messageObject[key],
          uid: key,
        }));

        this.setState({pacientes: messageList,
          loading: false });
      } else {
        this.setState({ pacientes: null, loading: false });
      }
    });
  }

  ConfirmarEliminar = () => {
    confirmAlert({
      title: 'Eliminar Paciente',
      message: 'Estás segur@ de eliminar este paciente?',
      buttons: [
        {
          label: 'Si',
          onClick: () => 
          {
              this.props.firebase.tratamiento(this.state.eliminar).remove();
              this.props.firebase.paciente(this.state.eliminar).remove();
            }
        },
        {
          label: 'No',
          onClick: () => console.log()
        }
      ]
    })


}

eliminarPaciente = (e) => {
    this.setState({eliminar: e.target.value
    });
    this.ConfirmarEliminar();
}


  render() {
    const { pacientes } = this.state;

    let filtro = this.state.pacientes.filter(
      (paciente) => {
        return paciente.nombre.indexOf(this.state.buscar)!==-1 || paciente.rut.indexOf(this.state.buscar)!== -1;
      }
    );

    return (
      <>
      <section class="breadcrumb_part breadcrumb_bg">
      <div class="container">
          <div class="row">
              <div class="col-lg-12">
                  <div class="breadcrumb_iner">
                      <div class="breadcrumb_iner_item">
                          <h2>Pacientes</h2>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </section>
      <section className="doctor_part single_page_doctor_part">
        <div className="container">

            <div className="busqueda row">
              <div className="col-sm-12 col-lg-9">
                  <InputGroup className="mb-3 hg100">
                  <InputGroup.Prepend>
                      <InputGroup.Text id="basic-addon1">Buscar Paciente</InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl
                      className="hg100"
                      name="buscar"
                      id="nombre-buscar"
                      placeholder="nombre o RUT de paciente"
                      aria-label="Username"
                      aria-describedby="basic-addon1"
                      value={this.state.buscar}
                      onChange={this.updateSearch.bind(this)}
                      
                  />
                  </InputGroup>            
              </div>
              <div className="buttons col-sm-12 col-lg-3">
                  <a className="btn_2 d-lg-block" href={ROUTES.INGRESARPACIENTE}>
                  <img src={mas}/> Agregar Paciente</a>
              </div>

            </div>


            {pacientes ? (
                <div className="row">
                  {filtro.map((message, i) => {
                      return(
                        <div className="col-sm-6 col-lg-3 paciente" key={message.uid} id={message.uid}>
                            <div className="single_blog_item">
                                <div className="single_blog_img">
                                    {message.sexo=="Femenino" ? (
                                      
                                        <img src={mujer} alt="paciente"/>
                                      
                                    ):(<img src={hombre} alt="paciente"/>)}
                                   

                                </div>
                                <div className="single_text">
                                    <div className="single_blog_text">
                                        <h3><b>{message.nombre}</b></h3>
                                        <p><img src={ubicacion} alt=""/><b>Dirección: </b>{message.direccion}</p>
                                        <p><img src={rut} alt=""/><b>Rut: </b>{message.rut}</p>
                                        <p><img src={telefono} alt=""/><b>Teléfono: </b>{message.telefono}</p>

                                    </div>
                                </div>
                                <a className="btn_2 d-lg-block col-sm-12" href={"/paciente/"+message.uid}><img src={ficha}/>Ver paciente</a>
                                <div className="buttons">
                                <Button className="btn_2 d-lg-block col-sm-12" variant="primary" value={message.uid} className="btn_2 d-lg-block" onClick={this.eliminarPaciente}>
                                        <img src={menos}/>Eliminar Paciente
                                        </Button>
                                </div>

                            </div>
                        </div>
                      )
                    })}
                </div>
            ) : (
                <></>
            )}
        </div>
      </section>
      </>
    );
  }
}






const condition = authUser =>
  authUser && !!authUser.roles[ROLES.ADMIN];

  export default compose(
    withAuthorization(condition),
    withFirebase,
  )(AdminPage);