import app from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';


const config = {
  apiKey: "AIzaSyA_2Vs8SxjqN0fVgoCaKsLJA3k6752JjLg",
  authDomain: "dentoplaza-98e3d.firebaseapp.com",
  databaseURL: "https://dentoplaza-98e3d.firebaseio.com",
  projectId: "dentoplaza-98e3d",
  storageBucket: "dentoplaza-98e3d.appspot.com",
  messagingSenderId: "1003673305053",
  appId: "1:1003673305053:web:3bf9d90def7b297b"
};

  class Firebase {
    constructor() {
      app.initializeApp(config);
      this.auth = app.auth();
      this.db = app.database();

    }

      // *** Auth API ***
      doCreateUserWithEmailAndPassword = (email, password) =>
      this.auth.createUserWithEmailAndPassword(email, password);

      doSignInWithEmailAndPassword = (email, password) =>
    this.auth.signInWithEmailAndPassword(email, password);

    doSignOut = () => {
    this.auth.signOut();
    }
    
   

    doPasswordReset = email => this.auth.sendPasswordResetEmail(email);

    doPasswordUpdate = password =>
    this.auth.currentUser.updatePassword(password);

// *** Merge Auth and DB User API *** //

onAuthUserListener = (next, fallback) =>
this.auth.onAuthStateChanged(authUser => {
  if (authUser) {
    this.user(authUser.uid)
      .once('value')
      .then(snapshot => {
        const dbUser = snapshot.val();

        // default empty roles
        if (!dbUser.roles) {
          dbUser.roles = {};
        }

        // merge auth and db user
        authUser = {
          uid: authUser.uid,
          email: authUser.email,
          ...dbUser,
        };

        next(authUser);
      });
  } else {
    fallback();
  }
});

    // *** User API ***

    user = uid => this.db.ref(`users/${uid}`);
    Roleuser = uid => this.db.ref(`users/${uid}/roles`);

    users = () => this.db.ref('users');

  // *** desayuno API ***

  desayuno = uid => this.db.ref(`desayunos/${uid}`);

  desayunos = () => this.db.ref('desayunos');
  

    // *** almuerzo API ***

    almuerzo = uid => this.db.ref(`almuerzos/${uid}`);

    almuerzos = () => this.db.ref('almuerzos');
    
  
    // *** sandwtich API ***

    sandwitch = uid => this.db.ref(`sandwitches/${uid}`);

    sandwitches = () => this.db.ref('sandwitches');

       // *** pedidos API ***

       pedido = uid => this.db.ref(`pedidos/${uid}`);
       pedidoUnico = (uid, idPro) => this.db.ref(`pedidos/${uid}/${idPro}`);
       pedidos = () => this.db.ref('pedidos');

       // *** productos API ***

       producto = (tipo, uid) => this.db.ref(`${tipo}/${uid}`);
       productos = (tipo) => this.db.ref(`${tipo}`);

       // pacientes
       pacientes = () => this.db.ref('pacientes');
       paciente = id => this.db.ref(`pacientes/${id}`);

       // tratamiento
       tratemientos = () => this.db.ref('tratamientos');
       tratamiento = id => this.db.ref(`tratamientos/${id}`);       
       tratamientoUsuario = (user, id) => this.db.ref(`tratamientos/${user}/${id}`);       

    }
  export default Firebase;
  