import React, { Component } from 'react'
import {Table, Form, Button, InputGroup} from 'react-bootstrap'
import { withAuthorization } from '../Session'
import { compose } from 'recompose'
import * as ROLES from '../../constants/roles'
import { withFirebase } from '../Firebase'
import mas from '../../imagenes/mas.png'
import paciente from '../../img/paciente.png'
import diente from '../../img/icon/feature_1.svg'
import calendario from '../../img/calendario.png'
import dinero from '../../img/dinero.png'
import ubicacion from '../../img/ubicacion.png'
import rut from '../../img/rut.png'
import sexo from '../../img/sexo.png'
import { confirmAlert } from 'react-confirm-alert'
import telefono from '../../img/telefono.png'
import estado from '../../img/estado.png'
import acciones from '../../img/acciones.png'


class Paciente extends Component {
  constructor(props) {
    super(props);

    this.state = {
        loading: false,
        nombre: '',
        direccion: '',
        rut: '',
        sexo: 'Masculino',
        tratamiento: '',
        precio:'',
        fecha:'',
        telefono: '',
        tratamientos: [],
        tratamientoEliminar:'',
        estado: 'Completo',

    };
  }

  onCreateMessage = event => {
    this.props.firebase.paciente(this.props.match.params.id).set({
        nombre: this.state.nombre,
        direccion: this.state.direccion,
        rut: this.state.rut,
        sexo: this.state.sexo,
        telefono: this.state.telefono
      });
  };

  formato(texto){
    return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
  }

  onCreateTratamiento = event => {
    this.props.firebase.tratamiento(this.props.match.params.id).push({
        tratamiento: this.state.tratamiento,
        precio: Number(this.state.precio).toLocaleString(),
        fecha: this.formato(this.state.fecha),
        estado: this.state.estado
      });

  };

  componentWillUnmount() {
    this.props.firebase.users().off();
  }


  
  componentDidMount() {

    this.setState({ loading: true });

    this.props.firebase.paciente(this.props.match.params.id).on('value', snapshot => {
      const messageObject = snapshot.val();
        this.setState({
          nombre: messageObject.nombre, 
          direccion:messageObject.direccion, 
          rut: messageObject.rut, 
          sexo: messageObject.sexo,
          telefono: messageObject.telefono});

    });

    this.props.firebase.tratamiento(this.props.match.params.id).on('value', snapshot => {
        const messageObject = snapshot.val();
  
        if (messageObject) {
          const messageList = Object.keys(messageObject).map(key => ({
            ...messageObject[key],
            uid: key,
          }));
  
          this.setState({tratamientos: messageList,
            loading: false });
        } else {
          this.setState({ tratamientos: null, loading: false });
        }
      });
  }

  onChangeText = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  onChangeSexo = event => {
    this.setState({ [event.target.name]: event.target.value });

  };

  onChangeEstado = event => {
    this.setState({ [event.target.name]: event.target.value });

  };

ConfirmarEliminar = () => {
  confirmAlert({
    title: 'Eliminar Tratamiento',
    message: 'Estás segur@ de eliminar este tratamiento?',
    buttons: [
      {
        label: 'Si',
        onClick: () => 
        {
          this.props.firebase.tratamientoUsuario(this.props.match.params.id, this.state.tratamientoEliminar).remove()
        }
      },
      {
        label: 'No',
        onClick: () => console.log()
      }
    ]
  })


}

eliminarTratamiento = (e) => {
  this.setState({tratamientoEliminar: e.target.value
  });
  this.ConfirmarEliminar();
}

terminarTratamiento = (e) => {
  this.props.firebase.tratamientoUsuario(this.props.match.params.id, e.target.value).update({
    estado: "Completo",
  })

}


   checkRut = event => {
    // Despejar Puntos
    var valor = event.target.value.replace('.','');
    // Despejar Guión
    valor = valor.replace('-','');
    
    // Aislar Cuerpo y Dígito Verificador
    var cuerpo = valor.slice(0,-1);
    var dv = valor.slice(-1).toUpperCase();
    
    // Formatear RUN
    event.target.value = cuerpo + '-'+ dv
    
    // Si no cumple con el mínimo ej. (n.nnn.nnn)
    if(cuerpo.length < 7) { event.target.setCustomValidity("RUT Incompleto"); return false;}
    
    // Calcular Dígito Verificador
    var suma = 0;
    var multiplo = 2;
    
    // Para cada dígito del Cuerpo
    for(var i=1;i<=cuerpo.length;i++) {
    
        // Obtener su Producto con el Múltiplo Correspondiente
        var index = multiplo * valor.charAt(cuerpo.length - i);
        
        // Sumar al Contador General
        suma = suma + index;
        
        // Consolidar Múltiplo dentro del rango [2,7]
        if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
  
    }
    
    // Calcular Dígito Verificador en base al Módulo 11
    var dvEsperado = 11 - (suma % 11);
    
    // Casos Especiales (0 y K)
    dv = (dv === 'K')?10:dv;
    dv = (dv === 0)?11:dv;
    
    // Validar que el Cuerpo coincide con su Dígito Verificador
    if(dvEsperado !== dv) { event.target.setCustomValidity("RUT Inválido"); return false; }
    
    // Si todo sale bien, eliminar errores (decretar que es válido)
    event.target.setCustomValidity('');
}



  render() {

    return (
      <>
      <section className="breadcrumb_part breadcrumb_bg">
      <div className="container">
          <div className="row">
              <div className="col-lg-12">
                  <div className="breadcrumb_iner">
                      <div className="breadcrumb_iner_item">
                          <h2>Paciente</h2>
                          <h3>{this.state.nombre}</h3>

                      </div>
                  </div>
              </div>
          </div>
      </div>
    </section>
      <section className="doctor_part single_page_doctor_part">
        <div className="container">
                <div className="row">
                        <div className="col-sm-12 col-lg-12 paciente"  >
                        <div className="text-center titulo">
                                <h2> Datos paciente</h2>
                            </div>
                            <Form
                                className=""
                                onSubmit={this.onCreateMessage}>  

                                <Form.Group controlId="formBasicNombre">
                                    <Form.Label><img src={paciente} alt=""/>Nombre</Form.Label>
                                    <Form.Control 
                                    name="nombre"
                                    required 
                                    type="text" 
                                    value={this.state.nombre}
                                    placeholder="Nombre paciente"
                                    onChange={this.onChangeText}/>
                                </Form.Group>

                                <Form.Group controlId="formBasicDescripcion">
                                    <Form.Label><img src={ubicacion} alt=""/>Dirección</Form.Label>
                                    <Form.Control 
                                    required
                                    name="direccion"
                                    type="text" 
                                    value={this.state.direccion}
                                    placeholder="Dirección paciente"
                                    onChange={this.onChangeText} />
                                </Form.Group>

                                <Form.Group controlId="formBasicPrecio">
                                    <Form.Label><img src={rut} alt=""/>RUT</Form.Label>
                                    <Form.Control
                                    aria-describedby="inputGroupPrepend" 
                                    type="text" 
                                    placeholder="RUT"
                                    required
                                    value={this.state.rut}
                                    name="rut"
                                    onChange={this.onChangeText}
                                    onInput={this.checkRut}/>
                                </Form.Group>

                                <Form.Group controlId="formBasicTelefono">
                                    <Form.Label><img src={telefono} alt=""/>Teléfono</Form.Label>
                                    <Form.Control
                                    aria-describedby="inputGroupPrepend" 
                                    type="tel" 
                                    placeholder="Teléfono"
                                    required
                                    value={this.state.telefono}
                                    name="telefono"
                                    onChange={this.onChangeText}/>
                                </Form.Group>

                                <Form.Group controlId="formBasicSexo">
                                    <Form.Label><img src={sexo} alt=""/>Sexo</Form.Label>

                                    <Form.Control value={this.state.sexo} name="sexo" onChange={this.onChangeSexo}  as="select">
                                      <option >Masculino</option>
                                      <option >Femenino</option>
                                      </Form.Control>
                                    </Form.Group>

                                <span className="explicacion">Asegurate de presionar el botón para modificar al paciente</span>
                                <div className="buttons">
                                        <Button variant="primary" className="btn_2 d-lg-block" type="submit">
                                        <img src={mas} alt="mas"/>Modificar Paciente
                                        </Button>

                                </div>
                            </Form>  

                            {this.state.tratamientos ? (
                                <>
                                <div className="text-center titulo">
                                <h2>Tratamientos </h2>
                                </div>                                
                                <Table responsive striped bordered hover >
                                <thead>
                                    <tr>
                                    <th><img src={calendario} alt=""/>Fecha</th>
                                    <th><img src={diente} alt=""/>Tratamiento</th>
                                    <th><img src={estado} alt=""/>Estado</th>
                                    <th><img src={dinero} alt=""/>Precio</th>
                                    <th><img src={acciones} alt=""/>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.tratamientos.map(tratamiento => (
                                    <tr key={tratamiento.uid}>
                                        <td> {tratamiento.fecha}</td>                                      
                                        <td> {tratamiento.tratamiento}</td>
                                        <td className="status"> <span className={tratamiento.estado}>{tratamiento.estado}</span></td>
                                        <td> ${tratamiento.precio}</td>
                                        <td className="acciones"><button type="submit" value={tratamiento.uid} className="btn_2 d-lg-block" onClick={this.eliminarTratamiento} >Eliminar
                                            </button>
                                            {tratamiento.estado == "Incompleto" ? (
                                              <button type="submit" value={tratamiento.uid} className="btn_2 d-lg-block" onClick={this.terminarTratamiento} >Terminar Tratamiento
                                              </button>                                              
                                            ):(<></>)
                                            }

                                        </td>
                                        
                                    </tr>
                                    ))}
                                </tbody>

                                </Table>
                                </>
                            ) : (
                                <>
                                </>
                            )}
                            <div className="text-center titulo">
                                <h2> Agregar Tratamiento</h2>
                            </div>
                            <Form
                                className=""
                                onSubmit={this.onCreateTratamiento}>  

                                <Form.Group controlId="formBasicNombre">
                                    <Form.Label><img src={diente} alt=""/>Tratamiento</Form.Label>
                                    <Form.Control 
                                    name="tratamiento"
                                    required 
                                    as="textarea"
                                    rows="3"
                                    value={this.state.tratamiento}
                                    placeholder="Tratamiento paciente"
                                    onChange={this.onChangeText}/>
                                </Form.Group>

                                <Form.Group controlId="formBasicFecha">
                                    <Form.Label><img src={calendario} alt=""/>Fecha</Form.Label>
                                    <Form.Control 
                                    name="fecha"
                                    required 
                                    type="date"
                                    value={this.state.fecha}
                                    placeholder="Fecha tratamiento"
                                    onChange={this.onChangeText}/>
                                </Form.Group>
                                

                                <Form.Group controlId="formBasicPrecio">
                                    <Form.Label><img src={dinero} alt=""/>Precio</Form.Label>
                                    <InputGroup>
                                            <InputGroup.Prepend>
                                            <InputGroup.Text id="inputGroupPrepend">$</InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <Form.Control
                                                aria-describedby="inputGroupPrepend" 
                                                type="number" 
                                                placeholder="Precio Tratamiento"
                                                required
                                                value={this.state.precio}
                                                name="precio"
                                                onChange={this.onChangeText}
                                                min="0" />
                                    </InputGroup>
                                </Form.Group>
                                <Form.Group controlId="formBasicEstado">
                                    <Form.Label><img src={estado} alt=""/>Estado tratamiento</Form.Label>

                                    <Form.Control value={this.state.estado} name="estado" onChange={this.onChangeEstado}  as="select">
                                      <option  >Completo</option>
                                      <option >Incompleto</option>
                                      </Form.Control>
                                    </Form.Group>
                                <span className="explicacion">Asegurate de presionar el botón para agregar el tratemiento</span>
                                <div className="buttons">
                                        <Button variant="primary" className="btn_2 d-lg-block" type="submit">
                                        <img src={mas}/>Agregar Tratamiento
                                        </Button>
                                </div>
                            </Form> 
                        </div>
                </div>
        </div>
      </section>
      </>
    );
  }
}






const condition = authUser =>
  authUser && !!authUser.roles[ROLES.ADMIN];

  export default compose(
    withAuthorization(condition),
    withFirebase,
  )(Paciente);