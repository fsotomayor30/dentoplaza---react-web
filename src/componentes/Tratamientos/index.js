import React, { Component } from 'react';
import {Table, Nav} from 'react-bootstrap';
import { withAuthorization } from '../Session';
import { compose } from 'recompose';
import * as ROLES from '../../constants/roles';
import { withFirebase } from '../Firebase';
import * as ROUTES from '../../constants/routes';
import { Redirect } from 'react-router-dom'
import Paciente from '../Paciente'
import diente from '../../img/icon/feature_1.svg';
import calendario from '../../img/calendario.png';
import dinero from '../../img/dinero.png';
import estado from '../../img/estado.png'

import {Card, Button, Row, Col, InputGroup, FormControl, Dropdown} from 'react-bootstrap';

import hombre from '../../img/paciente.png';
import mujer from '../../img/mujer.png';
import menos from '../../imagenes/eliminar.png';

import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css

class TratamientosPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tratamientos: [],

    };


  }



  componentWillUnmount() {
    this.props.firebase.users().off();
  }

  
  componentDidMount() {
    this.setState({ loading: true });
    this.props.firebase.tratamiento(this.props.match.params.id).on('value', snapshot => {
      const messageObject = snapshot.val();

      if (messageObject) {
        const messageList = Object.keys(messageObject).map(key => ({
          ...messageObject[key],
          uid: key,
        }));

        this.setState({tratamientos: messageList,
          });
      } else {
        this.setState({ tratamientos: null });
      }
    });
  }




  render() {
    return (
      <>
      <section class="breadcrumb_part breadcrumb_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner">
                        <div class="breadcrumb_iner_item">
                            <h2>Mis tratamientos</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </section>
      <section className="doctor_part single_page_doctor_part">
        <div className="container">
            {this.state.tratamientos ? (
                                <>
                                <div className="text-center titulo">
                                </div>                                
                                <Table responsive striped bordered hover >
                                <thead>
                                    <tr>
                                    <th><img src={calendario} alt=""/>Fecha</th>
                                    <th><img src={diente} alt=""/>Tratamiento</th>
                                    <th><img src={estado} alt=""/>Estado</th>
                                    <th><img src={dinero} alt=""/>Precio</th>
   
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.tratamientos.map(tratamiento => (
                                    <tr key={tratamiento.uid}>
                                        <td> {tratamiento.fecha}</td>
                                        <td> {tratamiento.tratamiento}</td>
                                        <td className="status"> <span className={tratamiento.estado}>{tratamiento.estado}</span></td>
                                        <td> ${tratamiento.precio}</td>

                                        
                                    </tr>
                                    ))}
                                </tbody>

                                </Table>
                                </>
                            ) : (
                                <>
                                </>
                            )}

            
        </div>
      </section>
      </>
    );
  }
}






const condition = authUser =>
  authUser ;

  export default compose(
    withAuthorization(condition),
    withFirebase,
  )(TratamientosPage);